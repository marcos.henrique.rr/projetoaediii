package view;

import com.google.gson.*;

import repository.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicArrowButton;

import java.text.DecimalFormat;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.awt.event.ActionEvent;

public class Compras {
	JFrame frmCompras;
	String manipula_lista_itens;
	static File dir_itens = new File(System.getProperty("user.dir") + "\\src\\repository\\itens");
	static File dir_listas = new File(System.getProperty("user.dir") + "\\src\\repository\\listas");
	static File dir_historico_listas = new File(
			System.getProperty("user.dir") + "\\src\\repository\\historico\\listas");
	static File dir_historico_itens = new File(System.getProperty("user.dir") + "\\src\\repository\\historico\\itens");
	DefaultListModel<String> arq_itens = new DefaultListModel<String>();
	DefaultListModel<String> arq_itens_lista = new DefaultListModel<String>();
	public Float soma_total;
	public Float soma_total_lista;
	DecimalFormat formatavalor = new DecimalFormat("0.00");
	public int contador_itens;
	public int contador_itens_lista;
	private JLabel lblDinNome;
	private JTextField txtFieldValor;
	private JTextField txtFieldQtd;

	/**
	 * Instancia a aplica��o.
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Compras window = new Compras();
					window.frmCompras.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Cria a aplica��o. *
	 */
	public Compras() {
		initialize();
	}

	/**
	 * Inicia o conte�do do frane.
	 * 
	 */
	private void initialize() {

		frmCompras = new JFrame();
		frmCompras.setResizable(false);
		frmCompras.setTitle("Compras");
		frmCompras.setBounds(100, 100, 290, 295);
		frmCompras.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCompras.getContentPane().setLayout(null);

		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(0, 11, 53, 21);
		lblNome.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNome.setHorizontalAlignment(SwingConstants.CENTER);
		frmCompras.getContentPane().add(lblNome);

		JList list_itens_carrinho = new JList(arq_itens);

		JScrollPane scroll_itens_carrinho = new JScrollPane();
		scroll_itens_carrinho.setBounds(167, 64, 97, 116);
		scroll_itens_carrinho.setViewportView(list_itens_carrinho);
		list_itens_carrinho.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_carrinho.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_carrinho.getSelectedValue().toString();
				}
			}
		});
		frmCompras.getContentPane().add(scroll_itens_carrinho);

		JList list_itens_lista = new JList(arq_itens_lista);

		JScrollPane scroll_itens_lista = new JScrollPane();
		scroll_itens_lista.setBounds(10, 64, 94, 116);
		scroll_itens_lista.setViewportView(list_itens_lista);
		list_itens_lista.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_lista.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_lista.getSelectedValue().toString();
					buscaValores();
				}
			}
		});

		frmCompras.getContentPane().add(scroll_itens_lista);

		JLabel lblItensDaLista = new JLabel("Itens da Lista");
		lblItensDaLista.setBounds(10, 43, 94, 21);
		lblItensDaLista.setHorizontalAlignment(SwingConstants.CENTER);
		lblItensDaLista.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmCompras.getContentPane().add(lblItensDaLista);

		JLabel lblItensNaLista = new JLabel("Carrinho");
		lblItensNaLista.setBounds(167, 43, 94, 21);
		lblItensNaLista.setHorizontalAlignment(SwingConstants.CENTER);
		lblItensNaLista.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmCompras.getContentPane().add(lblItensNaLista);

		JLabel lblTotaldeItens = new JLabel("Total de itens:");
		lblTotaldeItens.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotaldeItens.setBounds(9, 175, 94, 30);
		lblTotaldeItens.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmCompras.getContentPane().add(lblTotaldeItens);

		JLabel lblTotalR = new JLabel("Total Lista R$");
		lblTotalR.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotalR.setBounds(9, 200, 97, 30);
		lblTotalR.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmCompras.getContentPane().add(lblTotalR);

		JLabel total_itens = new JLabel("");
		total_itens.setHorizontalAlignment(SwingConstants.LEFT);
		total_itens.setFont(new Font("Arial Black", Font.PLAIN, 12));
		total_itens.setBounds(107, 175, 53, 30);
		frmCompras.getContentPane().add(total_itens);

		JLabel total_valor = new JLabel("");
		total_valor.setHorizontalAlignment(SwingConstants.LEFT);
		total_valor.setFont(new Font("Arial Black", Font.PLAIN, 12));
		total_valor.setBounds(107, 200, 53, 30);
		buscaLista();
		total_valor.setText(String.valueOf(soma_total_lista));

		frmCompras.getContentPane().add(total_valor);

		JLabel lblDinNome = new JLabel("");
		lblDinNome.setBounds(63, 15, 201, 14);
		lblDinNome.setText(data());
		frmCompras.getContentPane().add(lblDinNome);

		JLabel lblTotalCarrinho = new JLabel("");
		lblTotalCarrinho.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotalCarrinho.setFont(new Font("Arial Black", Font.PLAIN, 12));
		lblTotalCarrinho.setBounds(87, 225, 53, 30);
		frmCompras.getContentPane().add(lblTotalCarrinho);

		soma_total = (float) 0.00;

		BasicArrowButton btnAdicionaNaLista = new BasicArrowButton(BasicArrowButton.EAST);
		btnAdicionaNaLista.setFont(new Font("Tahoma", Font.PLAIN, 11));
		{
			btnAdicionaNaLista.setBounds(135, 63, 32, 23);
			btnAdicionaNaLista.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String nomeItem = manipula_lista_itens;
					FileReader arquivo = null;
					try {
						arquivo = new FileReader(
								System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} finally {
						Gson gson = new Gson();
						ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
						float temp = 0;
						if (Item.getQtd() != Integer.parseInt(txtFieldQtd.getText())
								|| Item.getValor() != Float.parseFloat(txtFieldValor.getText()) || Item.getQtd() > 1) {
							Item.setQtd(Integer.parseInt(txtFieldQtd.getText()));
							Item.setValor(Float.parseFloat(txtFieldValor.getText()));
							temp = (float) Item.getValor() * Item.getQtd();
						} else {
							temp = (float) Item.getValor();
						}
						
						historicoItem(Item.getNome(), Float.parseFloat(txtFieldValor.getText()),
								Integer.parseInt(txtFieldQtd.getText()));

						contador_itens = contador_itens + Item.getQtd();
						soma_total = soma_total + temp;
						total_itens.setText(String.valueOf(contador_itens));
						lblTotalCarrinho.setText(String.valueOf(soma_total));
						arq_itens.addElement(nomeItem);
					}
				}
			});
			frmCompras.getContentPane().add(btnAdicionaNaLista);
		}

		BasicArrowButton btnRemoveDaLista = new BasicArrowButton(BasicArrowButton.WEST);
		btnRemoveDaLista.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String nomeItem = manipula_lista_itens;
				FileReader arquivo = null;
				try {
					arquivo = new FileReader(
							System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} finally {
					Gson gson = new Gson();
					ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
					float temp = 0;
					if (Item.getQtd() != Integer.parseInt(txtFieldQtd.getText())
							|| Item.getValor() != Float.parseFloat(txtFieldValor.getText()) || Item.getQtd() > 1) {
						Item.setQtd(Integer.parseInt(txtFieldQtd.getText()));
						Item.setValor(Float.parseFloat(txtFieldValor.getText()));
						temp = (float) Item.getValor() * Item.getQtd();
					} else {
						temp = (float) Item.getValor();
					}

					if (contador_itens > 0 && soma_total > 0) {
						soma_total = soma_total - temp;
						contador_itens = contador_itens - Item.getQtd();
					}
					total_itens.setText(String.valueOf(contador_itens));
					lblTotalCarrinho.setText(String.valueOf(soma_total));
					arq_itens.removeElement(nomeItem);
				}
			}
		});
		btnRemoveDaLista.setBounds(104, 63, 32, 23);
		frmCompras.getContentPane().add(btnRemoveDaLista);

		JButton btnCancel = new JButton("Voltar");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelecionaListaCompras window = new SelecionaListaCompras();
				window.getFrmSelecionaLista().setVisible(true);
				frmCompras.dispose();

			}
		});
		btnCancel.setBounds(167, 224, 97, 21);
		frmCompras.getContentPane().add(btnCancel);
		JButton btnFinalizar = new JButton("Finalizar");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count = arq_itens.getSize();
				String[] vetorizacao = new String[count];
				for (int linha = 0; linha < count; linha++) {

					vetorizacao[linha] = arq_itens.getElementAt(linha);

				}
				String nome = ("\\" + lblDinNome.getText() + ".json");
				ListaRepo Lista = new ListaRepo(lblDinNome.getText(), Float.valueOf(lblTotalCarrinho.getText()),
						Integer.parseInt(total_itens.getText()), vetorizacao);

				try (Writer writer = new FileWriter(dir_historico_listas + nome)) {
					Gson gson = new GsonBuilder().create();
					gson.toJson(Lista, writer);
					sair();
				} catch (JsonIOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} finally {

				}
			}
		});
		btnFinalizar.setBounds(167, 191, 97, 30);
		frmCompras.getContentPane().add(btnFinalizar);

		JLabel lblTotalCompra = new JLabel("Carrinho R$");
		lblTotalCompra.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotalCompra.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblTotalCompra.setBounds(9, 225, 97, 30);
		frmCompras.getContentPane().add(lblTotalCompra);

		txtFieldValor = new JTextField();
		txtFieldValor.setBounds(107, 156, 58, 23);
		frmCompras.getContentPane().add(txtFieldValor);
		txtFieldValor.setColumns(10);

		txtFieldQtd = new JTextField();
		txtFieldQtd.setColumns(10);
		txtFieldQtd.setBounds(107, 110, 58, 23);
		frmCompras.getContentPane().add(txtFieldQtd);

		JLabel lblNewLabel = new JLabel("Qtd.");
		lblNewLabel.setBounds(111, 92, 58, 23);
		frmCompras.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Valor Unid.");
		lblNewLabel_1.setBounds(107, 139, 58, 23);
		frmCompras.getContentPane().add(lblNewLabel_1);

		list_itens_lista.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_lista.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_lista.getSelectedValue().toString();
				}
			}
		});
	}

	public void setfrmCompras(JFrame frmCompras) {
		this.frmCompras = frmCompras;
	}

	public JFrame getFrmCompras() {
		return frmCompras;
	}

	public static String removeExtension(String fileName) {
		if (fileName.indexOf(".") > 0) {
			return fileName.substring(0, fileName.lastIndexOf("."));
		} else {
			return fileName;
		}
	}

	public String data() {
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");

		return formatador.format(data);
	}

	private void buscaLista() {
		SelecionaListaCompras temp = new SelecionaListaCompras();
		String nomeLista = temp.getManipulaLista();
		FileReader arquivo = null;
		try {
			arquivo = new FileReader(
					System.getProperty("user.dir") + "\\src\\repository\\listas\\" + nomeLista + ".json");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			Gson gson = new Gson();
			ListaRepo Lista = gson.fromJson(arquivo, ListaRepo.class);

			soma_total_lista = Lista.getValorLista();
			contador_itens_lista = Lista.getQtd();
			this.converteArrayDefault(Lista.getItensLista());

		}
	}

	private void converteArrayDefault(String[] lista) {
		for (String nome : lista) {
			arq_itens_lista.addElement(nome);
		}
	}

	private void buscaValores() {
		String nomeItem = manipula_lista_itens;
		FileReader arquivo = null;
		try {
			arquivo = new FileReader(
					System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			Gson gson = new Gson();
			ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
			txtFieldValor.setText(String.valueOf(Item.getValor()));
			txtFieldQtd.setText(String.valueOf(Item.getQtd()));
		}
	}

	private void sair() {
		Principal window = new Principal();
		window.frmEverybuy.setVisible(true);
		frmCompras.dispose();
	}

	private void historicoItem(String Nome, Float valor, Integer Qtd) {
		String arquivo_nome = ("\\" + Nome + data()+".json");
		ItemRepo Item = new ItemRepo(Nome, valor, Qtd);

		try (Writer writer = new FileWriter(dir_historico_itens + arquivo_nome)) {
			Gson gson = new GsonBuilder().create();
			gson.toJson(Item, writer);

		} catch (JsonIOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}

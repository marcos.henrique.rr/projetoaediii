package view;

import com.google.gson.*;

import repository.*;

import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.text.DecimalFormat;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.awt.event.ActionEvent;

public class Lista_editar {
	static JFrame frmEditarLista;
	String manipula_lista_itens;
	static File dir_itens = new File(System.getProperty("user.dir") + "\\src\\repository\\itens");
	static File dir_listas = new File(System.getProperty("user.dir") + "\\src\\repository\\listas");
	DefaultListModel<String> arq_itens = new DefaultListModel<String>();
	DefaultListModel<String> arq_itens_lista = new DefaultListModel<String>();
	public Float soma_total;
	DecimalFormat formatavalor = new DecimalFormat("0.00");
	public int contador_itens;
	private JTextField nome_lista;
	private JTextField total_valor;
	private JTextField total_itens;

	/**
	 * Instancia a aplica��o.
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Object window = new Lista_editar();
					Lista_editar.frmEditarLista.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	{
		for (String p : dir_itens.list()) {
			arq_itens.addElement(removeExtension(p.toString()));
		}
	}

	/**
	 * Cria a aplica��o. *
	 */
	public Lista_editar() {
		initialize();
	}

	/**
	 * Inicia o conte�do do frane.
	 * 
	 */
	private void initialize() {

		frmEditarLista = new JFrame();
		frmEditarLista.setResizable(false);
		frmEditarLista.setTitle("Editar Lista");
		frmEditarLista.setBounds(100, 100, 290, 295);
		frmEditarLista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEditarLista.getContentPane().setLayout(null);

		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(0, 11, 53, 21);
		lblNome.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNome.setHorizontalAlignment(SwingConstants.CENTER);
		frmEditarLista.getContentPane().add(lblNome);

		nome_lista = new JTextField();
		nome_lista.setBounds(63, 12, 201, 20);
		frmEditarLista.getContentPane().add(nome_lista);
		nome_lista.setColumns(10);

		JList list_itens_lista = new JList(arq_itens_lista);

		JScrollPane scroll_itens_lista = new JScrollPane();
		scroll_itens_lista.setBounds(167, 64, 97, 116);
		scroll_itens_lista.setViewportView(list_itens_lista);
		list_itens_lista.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_lista.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_lista.getSelectedValue().toString();
				}
			}
		});
		frmEditarLista.getContentPane().add(scroll_itens_lista);

		JList list_itens_todos = new JList(arq_itens);

		JScrollPane scroll_total_itens = new JScrollPane();
		scroll_total_itens.setBounds(10, 64, 94, 116);
		scroll_total_itens.setViewportView(list_itens_todos);
		list_itens_todos.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_todos.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_todos.getSelectedValue().toString();
				}
			}
		});

		frmEditarLista.getContentPane().add(scroll_total_itens);

		JLabel lblTodosItens = new JLabel("Todos Itens");
		lblTodosItens.setBounds(10, 43, 94, 21);
		lblTodosItens.setHorizontalAlignment(SwingConstants.CENTER);
		lblTodosItens.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmEditarLista.getContentPane().add(lblTodosItens);

		JLabel lblItensNaLista = new JLabel("Itens na lista");
		lblItensNaLista.setBounds(167, 43, 94, 21);
		lblItensNaLista.setHorizontalAlignment(SwingConstants.CENTER);
		lblItensNaLista.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmEditarLista.getContentPane().add(lblItensNaLista);

		JLabel lblNewLabel = new JLabel("Total de itens:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setBounds(9, 190, 94, 30);
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmEditarLista.getContentPane().add(lblNewLabel);

		JLabel lblTotalR = new JLabel("Total R$");
		lblTotalR.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotalR.setBounds(10, 215, 97, 30);
		lblTotalR.setFont(new Font("Arial Black", Font.PLAIN, 11));
		frmEditarLista.getContentPane().add(lblTotalR);

		JLabel total_itens = new JLabel("");
		total_itens.setHorizontalAlignment(SwingConstants.LEFT);
		total_itens.setFont(new Font("Arial Black", Font.PLAIN, 12));
		total_itens.setBounds(107, 190, 53, 30);
		frmEditarLista.getContentPane().add(total_itens);

		JLabel total_valor = new JLabel("");
		total_valor.setHorizontalAlignment(SwingConstants.LEFT);
		total_valor.setFont(new Font("Arial Black", Font.PLAIN, 12));
		total_valor.setBounds(67, 215, 53, 30);

		frmEditarLista.getContentPane().add(total_valor);
		soma_total = (float) 0.00;

		JButton btnAdicionaNaLista = new JButton(">>");
		{
			btnAdicionaNaLista.setBounds(107, 75, 58, 23);
			btnAdicionaNaLista.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String nomeItem = manipula_lista_itens;
					FileReader arquivo = null;
					try {
						arquivo = new FileReader(
								System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} finally {
						Gson gson = new Gson();
						ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
						float temp = 0;
						if (Item.getQtd() > 1) {
							temp = (float) Item.getValor() * Item.getQtd();
						} else {
							temp = (float) Item.getValor();
						}
						contador_itens = contador_itens + Item.getQtd();
						soma_total = soma_total + temp;
						total_itens.setText(String.valueOf(contador_itens));
						total_valor.setText(String.valueOf(soma_total));
						arq_itens_lista.addElement(nomeItem);
					}
				}
			});
			frmEditarLista.getContentPane().add(btnAdicionaNaLista);
		}

		JButton btnRemoveDaLista = new JButton("<<");
		btnRemoveDaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeItem = manipula_lista_itens;
				FileReader arquivo = null;
				try {
					arquivo = new FileReader(
							System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} finally {
					Gson gson = new Gson();
					ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
					float temp = 0;
					if (Item.getQtd() > 1) {
						temp = (float) Item.getValor() * Item.getQtd();
					} else {
						temp = (float) Item.getValor();
					}
					if (contador_itens > 0 && soma_total > 0) {
						soma_total = soma_total - temp;
						contador_itens = contador_itens - Item.getQtd();
					}
					total_itens.setText(String.valueOf(contador_itens));
					total_valor.setText(String.valueOf(soma_total));
					arq_itens_lista.removeElement(nomeItem);
				}
			}
		});
		btnRemoveDaLista.setBounds(107, 109, 58, 23);
		frmEditarLista.getContentPane().add(btnRemoveDaLista);

		JButton btnCancel = new JButton("Voltar");
		btnCancel.addActionListener(new ActionListener() {		
				public void actionPerformed(ActionEvent e) {
					Lista window = new Lista();
					window.frmLista.setVisible(true);
					frmEditarLista.dispose();
					}
				});
		btnCancel.setBounds(167, 224, 97, 21);
		frmEditarLista.getContentPane().add(btnCancel);

		this.buscaLista();
		total_itens.setText(String.valueOf(contador_itens));
		total_valor.setText(String.valueOf(soma_total));
		JButton btnSave = new JButton("Salvar");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count = arq_itens_lista.getSize();
				String[] vetorizacao = new String[count];
				for (int linha = 0; linha < count; linha++) {
					vetorizacao[linha] = arq_itens_lista.getElementAt(linha);

				}
				String nome = ("\\" + nome_lista.getText() + ".json");
				ListaRepo Lista = new repository.ListaRepo(nome_lista.getText(),
						Float.valueOf(total_valor.getText()), Integer.parseInt(total_itens.getText()), vetorizacao);

				try (Writer writer = new FileWriter(dir_listas + nome)) {
					Gson gson = new GsonBuilder().create();
					gson.toJson(Lista, writer);
					total_itens.setText("");
					total_valor.setText("");
					nome_lista.setText("");
					arq_itens_lista.clear();

				} catch (JsonIOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSave.setBounds(167, 191, 97, 30);
		frmEditarLista.getContentPane().add(btnSave);

		list_itens_todos.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_itens_todos.getSelectedValue() != null) {
					manipula_lista_itens = list_itens_todos.getSelectedValue().toString();
				}
			}
		});
	}

	public void setfrmEditarLista(JFrame frmEditarLista) {
		Lista_editar.frmEditarLista = frmEditarLista;
	}

	public JFrame getFrmEditarLista() {
		return frmEditarLista;
	}

	public static String removeExtension(String fileName) {
		if (fileName.indexOf(".") > 0) {
			return fileName.substring(0, fileName.lastIndexOf("."));
		} else {
			return fileName;
		}
	}

	private void buscaLista() {
		Lista temp = new Lista();
		String nomeLista = temp.getManipulaLista();
		FileReader arquivo = null;
		try {
			arquivo = new FileReader(
					System.getProperty("user.dir") + "\\src\\repository\\listas\\" + nomeLista + ".json");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			Gson gson = new Gson();
			ListaRepo Lista = gson.fromJson(arquivo, ListaRepo.class);

			soma_total = Lista.getValorLista();
			contador_itens = Lista.getQtd();
			this.converteArrayDefault(Lista.getItensLista());
			nome_lista.setText(Lista.getNome());

		}
	}

	private void converteArrayDefault(String[] lista) {
		for (String nome : lista) {
			arq_itens_lista.addElement(nome);
		}
	}

}

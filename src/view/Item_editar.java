package view;

import java.awt.EventQueue;
import repository.*;
import com.google.gson.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;

public class Item_editar {

	static JFrame frmEditarItem;
	private JTextField textField_NomeItem;
	private JTextField textField_PrecoItem;
	private JTextField textField_QtdItem;
	static File dir_itens = new File(System.getProperty("user.dir") + "\\src\\repository\\itens");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Item_editar window = new Item_editar();
					window.frmEditarItem.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Item_editar() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEditarItem = new JFrame();
		frmEditarItem.setBounds(100, 100, 290, 295);
		frmEditarItem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEditarItem.getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNome.setBounds(10, 11, 46, 14);
		frmEditarItem.getContentPane().add(lblNome);
		
		textField_NomeItem = new JTextField();
		textField_NomeItem.setBounds(10, 30, 254, 20);
		frmEditarItem.getContentPane().add(textField_NomeItem);
		textField_NomeItem.setColumns(10);
		
		JLabel lblPreco = new JLabel("Pre\u00E7o");
		lblPreco.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblPreco.setBounds(10, 61, 46, 14);
		frmEditarItem.getContentPane().add(lblPreco);
		
		textField_PrecoItem = new JTextField();
		textField_PrecoItem.setBounds(10, 79, 254, 20);
		frmEditarItem.getContentPane().add(textField_PrecoItem);
		textField_PrecoItem.setColumns(10);
		
		JLabel lblQtd = new JLabel("Quantidade");
		lblQtd.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblQtd.setBounds(10, 110, 91, 14);
		frmEditarItem.getContentPane().add(lblQtd);
		
		textField_QtdItem = new JTextField();
		textField_QtdItem.setBounds(10, 135, 254, 20);
		frmEditarItem.getContentPane().add(textField_QtdItem);
		textField_QtdItem.setColumns(10);
		
		this.buscaValores();
		
		JButton btnEditarItem = new JButton("Salvar");
		btnEditarItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String arquivo_nome = ("\\"+textField_NomeItem.getText()+".json");
				ItemRepo Item = new ItemRepo(textField_NomeItem.getText(),Float.valueOf(textField_PrecoItem.getText()), Integer.parseInt(textField_QtdItem.getText()));
				
				try (Writer writer = new FileWriter(dir_itens+arquivo_nome)) {
					    Gson gson = new GsonBuilder().create();
					    gson.toJson(Item, writer);
					    textField_PrecoItem.setText("");
					    textField_QtdItem.setText("");
					    textField_NomeItem.setText("");
						
				} catch (JsonIOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
								  
			}
		});
		btnEditarItem.setBounds(10, 184, 91, 61);
		frmEditarItem.getContentPane().add(btnEditarItem);
		
		JButton btnCancelarEditarItem = new JButton("Voltar");
		btnCancelarEditarItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Item window = new Item();
				window.frmItem.setVisible(true);
				frmEditarItem.dispose();
			}
		});
		btnCancelarEditarItem.setBounds(173, 184, 91, 61);
		frmEditarItem.getContentPane().add(btnCancelarEditarItem);
	}
	
	public JFrame getFrmEditarItem() {
		return frmEditarItem;
	}
	
	private void buscaValores() {
		view.Item temp = new view.Item(); 
		String nomeItem = temp.getManipulaItem();
		FileReader arquivo = null;
		try {
			arquivo = new FileReader(
					System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nomeItem + ".json");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			Gson gson = new Gson();
			ItemRepo Item = gson.fromJson(arquivo, ItemRepo.class);
			textField_PrecoItem.setText(String.valueOf(Item.getValor()));
		    textField_QtdItem.setText(String.valueOf(Item.getQtd()));
		    textField_NomeItem.setText(Item.getNome());					
		}
	}
}

package view;

import repository.*;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.List;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollBar;

public class Lista {
    JFrame frmLista;
	static String manipula_lista;
	static File dir_listas = new File(System.getProperty("user.dir") + "\\src\\repository\\listas");

    DefaultListModel<String> arq_listas = new DefaultListModel<String>();

	/**
	 * Instancia a aplica��o.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Lista window = new Lista();
					window.getFrmLista().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	{
		for (String p : dir_listas.list()) {
			arq_listas.addElement(removeExtension(p.toString()));
		}
	}

	/**
	 * Cria a aplica��o.
	 */
	public Lista() {
		initialize();
	}

	public JFrame getFrmLista() {
		return frmLista;
	}

	/**
	 * Inicia o conte�do do frane.
	 */

	private void initialize() {
		JFrame Listas = new JFrame();
		Listas.setTitle("Listas");
		setFrmLista(Listas);
		Listas.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Novo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Lista_novo lista_novo = new Lista_novo();
				lista_novo.getFrmListaNovo().setVisible(true);
				
					frmLista.dispose();
				
			}
		});
		btnNewButton.setBounds(10, 23, 112, 37);
		Listas.getContentPane().add(btnNewButton);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Lista_editar lista_editar = new Lista_editar();
				lista_editar.getFrmEditarLista().setVisible(true);
				
					frmLista.dispose();
				
			}
		});
		btnEditar.setBounds(10, 71, 112, 37);
		Listas.getContentPane().add(btnEditar);

		JButton btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeLista = manipula_lista;

				File arquivo = new File(System.getProperty("user.dir") + "\\src\\repository\\listas\\" + nomeLista+".json");
				arquivo.delete();
				arq_listas.removeElement(manipula_lista);
			}
		});

		btnApagar.setBounds(10, 119, 112, 37);
		Listas.getContentPane().add(btnApagar);

		JList list = new JList(arq_listas);

		JScrollPane scrollListas = new JScrollPane();
		scrollListas.setBounds(132, 23, 129, 190);
		scrollListas.setViewportView(list);

		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list.getSelectedValue() != null) {
					manipula_lista = list.getSelectedValue().toString();
				}
			}
		});

		Listas.getContentPane().add(scrollListas);

		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal window = new Principal();
				window.frmEverybuy.setVisible(true);
				frmLista.dispose();
			}
		});
		btnVoltar.setBounds(10, 176, 112, 37);
		Listas.getContentPane().add(btnVoltar);

		getFrmLista().setTitle("Listas");
		frmLista.setBounds(100, 100, 290, 295);
		frmLista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLista.getContentPane().setLayout(null);

	}

	public void setFrmLista(JFrame frmLista) {
		this.frmLista = frmLista;
		frmLista.setBounds(100, 100, 290, 295);
	}
	
	public String getManipulaLista() {
		return this.manipula_lista;
	}
	
	public static String removeExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        } 
    }

}

package view;

import java.awt.EventQueue;
import java.awt.Window;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SelecionaListaCompras {

	private JFrame SelecionaLista;
	static String manipula_lista;
	static File dir_listas = new File(System.getProperty("user.dir") + "\\src\\repository\\listas");
    DefaultListModel<String> arq_listas = new DefaultListModel<String>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelecionaListaCompras window = new SelecionaListaCompras();
					window.SelecionaLista.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	{
		for (String p : dir_listas.list()) {
			arq_listas.addElement(removeExtension(p.toString()));
		}
	}

	/**
	 * Create the application.
	 */
	public SelecionaListaCompras() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		SelecionaLista = new JFrame();
		SelecionaLista.setResizable(false);
		SelecionaLista.setTitle("Selecione");
		SelecionaLista.setBounds(100, 100, 290, 295);
		SelecionaLista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SelecionaLista.getContentPane().setLayout(null);
		
		JList list = new JList(arq_listas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 254, 187);
		scrollPane.setViewportView(list);
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list.getSelectedValue() != null) {
					manipula_lista = list.getSelectedValue().toString();
				}
			}
		});
		SelecionaLista.getContentPane().add(scrollPane);
		
		JButton btnSeleciona = new JButton("Selecionar e Ir");
		btnSeleciona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Compras compras = new Compras();
				compras.getFrmCompras().setVisible(true);				
				SelecionaLista.dispose();
			}
		});
		btnSeleciona.setBounds(10, 209, 132, 36);
		SelecionaLista.getContentPane().add(btnSeleciona);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal window = new Principal();
				window.frmEverybuy.setVisible(true);
				SelecionaLista.dispose();
			}
		});
		btnVoltar.setBounds(152, 209, 112, 36);
		SelecionaLista.getContentPane().add(btnVoltar);
	}
	
	public String getManipulaLista() {
		return this.manipula_lista;
	}
	
	public static String removeExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        } 
    }

	public JFrame getFrmSelecionaLista() {
		return SelecionaLista;
	}
}

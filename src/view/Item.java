package view;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Item {
    JFrame frmItem;
    static String manipula_item;
	static File dir_itens = new File(System.getProperty("user.dir") + "\\src\\repository\\itens");
    DefaultListModel<String> arq_itens = new DefaultListModel<String>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Item window = new Item();
					window.frmItem.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	{
		for (String p : dir_itens.list()) {
			arq_itens.addElement(removeExtension(p.toString()));
		}
	}
	
	/**
	 * Create the application.
	 */
	public Item() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frmItem = new JFrame();
		frmItem.setTitle("Itens");
		frmItem.setBounds(100, 100, 290, 295);
		frmItem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmItem.getContentPane().setLayout(null);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.Item_novo ItemNovo = new view.Item_novo();
				ItemNovo.getFrmNovoItem().setVisible(true);
				frmItem.dispose();
			}
		});
		btnNovo.setBounds(10, 23, 112, 37);
		frmItem.getContentPane().add(btnNovo);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.Item_editar ItemEditar = new view.Item_editar();
				ItemEditar.getFrmEditarItem().setVisible(true);
				frmItem.dispose();
			}
		});
		btnEditar.setBounds(10, 71, 112, 37);
		frmItem.getContentPane().add(btnEditar);
		
		JButton btnA = new JButton("Apagar");
		btnA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome_item = manipula_item;

				File arquivo = new File(System.getProperty("user.dir") + "\\src\\repository\\itens\\" + nome_item + ".json");
				arquivo.delete();
				arq_itens.removeElement(manipula_item);
			}
		});
		btnA.setBounds(10, 119, 112, 37);
		frmItem.getContentPane().add(btnA);
		
		JButton btnNewButton = new JButton("Voltar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal window = new Principal();
				window.frmEverybuy.setVisible(true);
				frmItem.dispose();
			}
		});
		btnNewButton.setBounds(10, 176, 112, 37);
		frmItem.getContentPane().add(btnNewButton);
		
		
		JList list_item = new JList(arq_itens);

		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(132, 23, 129, 190);
		scrollPane.setViewportView(list_item);
		
		list_item.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				System.out.println(e);
				if (list_item.getSelectedValue() != null) {
					manipula_item = list_item.getSelectedValue().toString();
				}
			}
		});
		
		frmItem.getContentPane().add(scrollPane);

	}
	
	public static String removeExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        } 
    }
	
	public JFrame getFrmItem() {
		return frmItem;
	}
	
	public String getManipulaItem() {
		return this.manipula_item;
	}
}

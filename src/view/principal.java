package view;

import java.awt.EventQueue;
import javax.swing.*;
import java.io.*;
import java.text.*;
import java.awt.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Window;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal {
	JFrame frmEverybuy;
	/**
	 * Instancia a aplica��o.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmEverybuy.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}	

	/**
	 * Cria a aplica��o.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Inicia o conte�do do frame.
	 */
	private void initialize() {
		frmEverybuy = new JFrame();
		frmEverybuy.setResizable(false);
		frmEverybuy.setTitle("TrabalhoAEDIII");
		frmEverybuy.setBounds(100, 100, 290, 295);
		frmEverybuy.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEverybuy.getContentPane().setLayout(null);

		JLabel lblOrganizadorDeCompras = new JLabel("Organizador de Compras");
		lblOrganizadorDeCompras.setBounds(0, 0, 283, 17);
		lblOrganizadorDeCompras.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblOrganizadorDeCompras.setHorizontalAlignment(SwingConstants.CENTER);
		frmEverybuy.getContentPane().add(lblOrganizadorDeCompras);

		JButton btnListas = new JButton("Listas");
		btnListas.setBounds(10, 28, 119, 51);
		btnListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Lista listas = new Lista();
				listas.getFrmLista().setVisible(true);
				{

					frmEverybuy.dispose();

				}
			}
		});
		frmEverybuy.getContentPane().add(btnListas);

		JButton btnItens = new JButton("Itens");
		btnItens.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Item item = new Item();
					item.getFrmItem().setVisible(true);
				{

					frmEverybuy.dispose();

				}
			}
		});
		btnItens.setBounds(10, 99, 119, 51);
		frmEverybuy.getContentPane().add(btnItens);

		JButton btnHistrico = new JButton("Hist\u00F3rico");
		btnHistrico.setBounds(10, 167, 119, 51);
		frmEverybuy.getContentPane().add(btnHistrico);

		JButton btnIrAsCompras = new JButton("Ir as compras");
		btnIrAsCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelecionaListaCompras SelecionaLista = new SelecionaListaCompras();
				SelecionaLista.getFrmSelecionaLista().setVisible(true);
			{

				frmEverybuy.dispose();

			}
			}
		});
		btnIrAsCompras.setBounds(139, 28, 119, 190);
		frmEverybuy.getContentPane().add(btnIrAsCompras);
	}
	
	
}

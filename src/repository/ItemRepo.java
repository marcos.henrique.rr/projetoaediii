package repository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ItemRepo {

	private float Valor;
	private int Qtd;
	private String Nome;

	public ItemRepo(String Nome, float valor, int Qtd) {
		this.Nome = Nome;
		this.Qtd = Qtd;
		this.setValor(valor);
		
	}

	public int getQtd() {
		return Qtd;
	}

	public void setQtd(int Qtd) {
		this.Qtd = Qtd;
	}

	public float getValor() {
		return Valor;
	}
	
	public String getNome() {
		return Nome;
	}
	
	public void setNome(String nome) {
		this.Nome = nome;
	}
	public void setValor(float valor) {
		this.Valor = valor;
	}
	

}

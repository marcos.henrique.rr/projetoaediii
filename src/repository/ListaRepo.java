package repository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;

public class ListaRepo {

		private float valor_Lista;
		private int qtd_itens_lista;
		private String nome_lista;
		private String[] lista;

		public ListaRepo(String nome_lista, Float valor_Lista, int qtd_itens_lista, String[] lista) {
			this.valor_Lista = valor_Lista;
			this.qtd_itens_lista = qtd_itens_lista;
			this.nome_lista = nome_lista;
			this.lista = lista;
		}

		public int getQtd() {
			return qtd_itens_lista;
		}
		
		public void setItensLista(String[] lista) {
			this.lista = lista;
		}
		
		public String[] getItensLista() {
			return lista;
		}

		public void setQtdItensLista(int Qtd) {
			this.qtd_itens_lista = Qtd;
		}

		public float getValorLista() {
			return valor_Lista;
		}
		
		public String getNome() {
			return nome_lista;
		}
		
		public void setNome(String nome) {
			this.nome_lista = nome;
		}
		public void setValor(float valor) {
			this.valor_Lista = valor;
		}
}
